#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

typedef struct Node Node;

struct Node
{
    char *data;
    struct Node *link;
};
struct Node *top;


void push(char *str);
bool isEmpty();
void display();
void pop();
