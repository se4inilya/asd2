#include "parse_html.h"

void Parse_html(char *filename)
{

    FILE *fs = fopen(filename, "r");
    if (fs == NULL)
    {
        exit(1);
    }

    char tempArr[100];
    char *ptr = tempArr;
    bool cFlag = false; //flag for continuing after reading closed tag
    while (*ptr != EOF)
    {
        cFlag = false;
        *ptr = fgetc(fs);

        if (*ptr == '<') //reading a tag
        {
            while (*ptr != '>')
            {

                if (*ptr == '/') //if closing tag
                {
                    ptr = tempArr;
                    *ptr = '<';
                    while (*ptr != '>')
                    {

                        ptr++;
                        *ptr = fgetc(fs);
                    }
                    ptr++;
                    *ptr = '\0';
                    if(strcmp(top->data, tempArr)!= 0){
                        printf("\n Wrong closing tag, Aborting...\n");
                        exit(1);
                    }
                    printf("%s\n", tempArr);
                    pop();// pop from stack if closing tag
                    printf("\nElement was popped!\n");
                    display();
                    ptr = tempArr;
                    cFlag = true;
                    break;
                }
                
                ptr++;
                *ptr = fgetc(fs);
            }
            if (cFlag == true) //for skipping push after closing tag
                {
                    continue;
                }
            ptr++;
            *ptr = '\0';
            push(tempArr); //push on stack if openning tag
            ptr = tempArr;
            printf("\nElement was pushed!\n");
            display();
        }
        ptr++;
        ptr = tempArr;
    }

    fclose(fs);

    if (isEmpty())
    {
        puts("");
        printf("Html tags are balanced");
    }
    else
    {
        puts("");
        printf("Tags not balanced, unclosed openning tag/tags");
    }
}
