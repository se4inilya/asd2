#include "linked_stack.h"



void push(char *str) 
{ 
    struct Node* temp; 
    temp = (struct Node*)malloc(sizeof(struct Node)); 

    //add string to node
    temp->data = (char *)malloc(sizeof(char) * strlen(str) + 1);
    strcpy(temp->data, str);
  
    //link
    temp->link = top; 
  
    // put on top
    top = temp; 
} 

bool isEmpty() 
{ 
    return top == NULL;
} 
  
  void pop() 
{ 
    struct Node* temp; 
  
    //if stack already empty, this means that we have extra closing tag
    if (top == NULL) { 
        printf("\nERROR! Stack already Empty, an unnecessary closing tag"); 
        exit(1); 
    } 
    else { 
        // top assign into temp 
        temp = top; 
  
        // make the next element top
        top = top->link; 
  
        // destroy connection for previous top
        temp->link = NULL; 
  
        // release memory of top node 
        free(temp->data);
        free(temp); 
    } 
} 
  
void display() 
{ 
    struct Node* temp; 
  
    // check if stack is empty
    if (top == NULL) { 
        printf("\nStack Empty\n"); 
    } 
    else { 
        temp = top; 
        printf("\n Stack:\n");
        while (temp != NULL) { 
  
            // print node data 
            
            printf("%s,  ", temp->data);
  
            //iterate through stack
            temp = temp->link; 
        } 
        puts("");
    } 
} 