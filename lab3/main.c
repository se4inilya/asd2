#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <progbase.h>
#include <string.h>

#include "hashtable.h"



int main()
{
    Author authorsarray[] = {{0, "Alexander Duma"}, {1, "Agata Kristi"}, {2, "Stendal"}, {3, "Victor Gugo"}};
    int autharrsize = sizeof(authorsarray) / sizeof(authorsarray[0]);
    List *list1 = List_alloc();
    for (int i = 0; i < autharrsize; i++)
    {
        List_add(list1, strOnHeap(authorsarray[i].name));
    }
    for (int i = 0; i < List_size(list1); i++)
    {
        printf("%s\n", (char *)List_get(list1, i));
    }
    printf("List size: %i\n", (int)List_size(list1));
    PubT array1[10] = {
        {"Maxim"},
        {"Playboy"},
        {"Happy Housewife"},
        {"Agriculture"},
        {"Peace"},
        {"Evil near to us"},
        {"History"},
        {"Powerful Cars"},
        {"Influence Of Net"},
        {"Dragons"}};
    PubInfoT array2[10] = {
        {"Maxim", list1, "Katrin Guseva", 2003, 20},
        {"Playboy", list1, "Marlin Monro", 1954, 4},
        {"Happy Housewife", list1, "tyty", 2000, 5},
        {"Agriculture", list1, "Combine", 1961, 44},
        {"Peace", list1, "Palestina", 2009, 2},
        {"Evil near to us", list1, "Terrorists", 2012, 1},
        {"History", list1, "Revolution", 2013, 3},
        {"Powerful Cars", list1, "Dodge Charger", 1985, 11},
        {"Influence Of Net", list1, "Serials", 2007, 6},
        {"Dragons", list1, "Drogan", 2019, 3},
    };
    Links array3[] = {
        {"Maxim", "1"},
        {"Playboy", "3"},
        {"Playboy", "1"},
        {"Happy Housewife", "1"},
        {"Agriculture", "2"},
        {"Peace", "0"},
        {"Evil near to us", "1"},
        {"History", "2"},
        {"Powerful Cars", "3"},
        {"Influence Of Net", "0"},
        {"Dragons", "1"},
        {"Evil near to us", "3"},
        {"Dragons", "0"},
        {"Influence Of Net", "2"},
        {"Playboy", "0"},
        {"Playboy", "2"},
        {"Peace", "1"}};
    HashtableTP table1 = initHashtable();
    int arrsize = sizeof(array1) / sizeof(array1[0]);
    int arr3size = sizeof(array3) / sizeof(array3[0]);
    for (int i = 0; i < arrsize; i++)
    {
        insert(table1, StructKey(array1[i]), StructValue(array2[i]));
    }
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        NodeTP entry = table1->table[i];
        while (entry != NULL)
        {
            char *key = entry->key->doi;
            for (int j = 0; j < arr3size; j++)
            {
                char *arr3key = array3[j].pt;
                if (strcmp(key, arr3key) == 0)
                {
                    List_add(entry->value->authors, strOnHeap(array3[j].au));
                }
            }
            entry = entry->next;
        }
    }

    while (1)
    {
        puts("\nFunctions : \n1.Show the table\n2.Find by key\n3.Delete by key\n4.Authors list\n5.Hirsh index of---\n6.Quit");
        int op = 0;
        scanf("%i", &op);
        getchar();
        if (op == 1)
        {
            printTable(table1, list1);
        }
        if (op == 2)
        {
            puts("Enter the key!!");
            char input[100];
            fgets(input, 100, stdin);
            input[strlen(input) - 1] = '\0';
            HashtableKeyTP tmpkey = StructKey((PubT){input});
            if (contains(table1, tmpkey) == 0)
            {
                puts("There is no such element!!!");
            }
            else
            {
                puts("Found 'em!");
                printInstance(find(table1, tmpkey), list1);
            }
            free(tmpkey);
        }
        if (op == 3)
        {
            puts("Enter the element what you want to delete:");
            char input[100];
            fgets(input, 100, stdin);
            input[strlen(input) - 1] = '\0';
            HashtableKeyTP tmpkey = StructKey((PubT){input});
            if (contains(table1, tmpkey) == 0)
            {
                puts("There is no such element!!!");
            }
            else
            {
                removeEl(table1, tmpkey);
                puts("The element is deleted");
            }
            free(tmpkey);
        }
        if (op == 5)
        {
            puts("Enter the key");
            char input[100];
            fgets(input, 100, stdin);
            input[strlen(input) - 1] = '\0';
            HashtableKeyTP tmpkey = StructKey((PubT){input});
            authorsHIndex(table1, tmpkey, list1);
            free(tmpkey);
        }
        if (op == 4)
        {
            List_print(list1);
        }
        if (op == 6)
        {
            break;
        }
    }
    List_clear(list1);
    List_free(list1);
    FreeLists(table1);
    deinitHashTable(table1);
    return 0;
}
