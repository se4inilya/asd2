#include "list.h"

#define HASHTABLE_CAPACITY 5
#define PRIME 37
#define SEED 173

struct __Author
{
    int index;
    char *name;
};
typedef struct __Author Author;

struct __Links
{
    char *pt;
    char *au;
};
typedef struct __Links Links;



typedef struct Pub
{
    char *doi;
} PubT;
typedef PubT *PubTP;
typedef struct PublInformation
{
    char *title;
    List *authors;
    char *journalName;
    int yearOfPublishing;
    int citingIndex;
} PubInfoT;
typedef PubInfoT *PubInfoTP;

typedef PubTP HashtableKeyTP;
typedef PubInfoTP HashtableValueTP;

typedef struct NodeA
{
    int index;
    struct NodeA *next;
} NodeAT;
typedef NodeAT *NodeATP;

typedef struct Node
{
    HashtableKeyTP key;
    HashtableValueTP value;
    struct Node *next;
} NodeT;
typedef NodeT *NodeTP;

NodeTP createNode(HashtableKeyTP key, HashtableValueTP value);
void destroyNode(NodeTP node);

typedef struct Hashtable
{
    NodeTP table[HASHTABLE_CAPACITY];
    int loadness;
    int size;
} HashtableT;
typedef HashtableT *HashtableTP;

int getAuthor(const char *auth, char *ptr, int *arrs);
void printAuthors(char *ptr, int arrs);

HashtableTP initHashtable();
NodeTP insert(HashtableTP dic, HashtableKeyTP key, HashtableValueTP value);
int removeEl(HashtableTP dic, HashtableKeyTP key);
HashtableValueTP find(HashtableTP dic, HashtableKeyTP key);
int contains(HashtableTP dic, HashtableKeyTP key);
int isEmpty(HashtableTP dic);
int size(HashtableTP dic);
int loadness(HashtableTP dic);
void clear(HashtableTP dic);
void printTable(HashtableTP dic, List *authors);
void printInstance(HashtableValueTP m, List *authors);
void deinitHashTable(HashtableTP dic);
void authorsHIndex(HashtableTP dic, HashtableKeyTP key, List *authors);

PubTP StructKey(PubT key);
PubInfoTP StructValue(PubInfoT value);
List *CreateAuthorsList(PubTP key);
unsigned int keyHashCode(PubTP pub);
unsigned int hashCodeString(char *str);
void Addauthor(List *list1, NodeTP entry, char *authorinput);
void FreeLists(HashtableTP dic);
void insertionSort(int arr[], int n);
void reverse_array(int *pointer, int n);
void bubbleSort(int arr[], int n);
void swap(int *xp, int *yp);