#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include <string.h>

static void List_realloc(List *self)
{
    self->items = (void **)realloc(self->items, sizeof(void *) * self->capacity * 2);
    if (self->items == NULL)
    {
        fprintf(stderr, "Allocation error");
        abort();
    }
    self->capacity *= 2;
}

List *List_alloc(void)
{
    List *list = malloc(sizeof(List));

    List_init(list);
    return list;
}

void List_init(List *self)
{
    self->capacity = 16;
    self->size = 0;
    self->items = malloc(sizeof(void *) * self->capacity);
    if (self->items == NULL)
    {
        fprintf(stderr, "Allocation error");
        abort();
    }
}

void List_free(List *self)
{
    List_deinit(self);
    free(self);
}
void List_deinit(List *self)
{
    free(self->items);
}
int List_size(List *self)
{
    return self->size;
}
void *List_get(List *self, int index)
{
    return self->items[index];
}
void List_set(List *self, int index, void *value)
{
    self->items[index] = value;
}
void List_add(List *self, void *value)
{
    if (self->size >= self->capacity)
    {
        List_realloc(self);
    }
    self->items[self->size] = value;
    self->size++;
}
void List_clear(List *self)
{
    for (int i = 0; i < self->size; i++)
    {
        free(self->items[i]);
    }
}
void List_print(List *self)
{
    for (int i = 0; i < self->size; i++)
    {
        puts(self->items[i]);
    }
    puts("");
}
void List_removeAt(List *self, int index)
{
    for (int i = index; i < self->size; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->size--;

} // remove and shift left
void *strOnHeap(const char *str)
{
    char *m = malloc(strlen(str) + 1);
    strcpy(m, str);
    return m;
}