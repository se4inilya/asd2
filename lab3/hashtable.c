#include "hashtable.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

PubTP StructKey(PubT key)
{
    PubTP m = (PubTP)malloc(sizeof(PubT));
    m->doi = key.doi;
    return m;
}
PubInfoTP StructValue(PubInfoT value)
{
    PubInfoTP m = (PubInfoTP)malloc(sizeof(PubInfoT));
    m->title = value.title;
    m->authors = List_alloc();
    m->journalName = value.journalName;
    m->yearOfPublishing = value.yearOfPublishing;
    m->citingIndex = value.citingIndex;
    return m;
}

HashtableTP initHashtable()
{
    HashtableTP emptyTable = (HashtableTP)malloc(sizeof(HashtableT));
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        emptyTable->table[i] = NULL;
    }
    emptyTable->loadness = 0;
    emptyTable->size = 0;
    return emptyTable;
}

NodeTP createNode(HashtableKeyTP key, HashtableValueTP value)
{
    NodeTP node = (NodeTP)malloc(sizeof(NodeT));
    node->key = key;
    node->value = value;
    node->next = NULL;
    return node;
}

static int getHash(HashtableKeyTP key)
{
    unsigned int hashCode = keyHashCode(key);
    int tableMaxPosition = HASHTABLE_CAPACITY - 1;
    int hash = hashCode % tableMaxPosition;
    // printf("%i\n", hash);
    return hash;
}

unsigned int keyHashCode(PubTP pub)
{
    unsigned int hash = SEED;
    hash = PRIME * hash + hashCodeString(pub->doi);
    // printf("[[%u]]\n", hash);
    return hash;
}

unsigned int hashCodeString(char *str)
{
    unsigned int hash = SEED;
    for (int i = 0; i < strlen(str); i++)
    {
        hash = PRIME * hash + str[i];
    }
    return hash;
}

NodeTP insert(HashtableTP dic, HashtableKeyTP key, HashtableValueTP value)
{
    NodeTP node = createNode(key, value);
    int hash = getHash(key);
    NodeTP entry = dic->table[hash];
    if (entry == NULL)
    {
        dic->table[hash] = node;
        dic->loadness++;
        dic->size++;
    }
    else
    {
        while (entry->next != NULL)
        {
            if (strcmp(entry->key->doi, key->doi) == 0)
            {
                entry->value = value;
                return node;
            }
            entry = entry->next;
        }
        if (strcmp(entry->key->doi, key->doi) == 0)
        {
            entry->value = value;
        }
        else
        {
            entry->next = node;
            dic->size++;
        }
    }
    return node;
}

int removeEl(HashtableTP dic, HashtableKeyTP key)
{
    int hash = getHash(key);
    NodeTP entry = dic->table[hash];
    if (entry == NULL)
    {
        return 0;
    }
    if (strcmp(entry->key->doi, key->doi) == 0)
    {
        dic->table[hash] = entry->next;
        List_free(entry->value->authors);
        destroyNode(entry);
        dic->size--;
        if (dic->table[hash] == NULL)
        {
            dic->loadness--;
        }
        return 1;
    }
    NodeTP prev = entry;
    NodeTP current = entry->next;
    while (current != NULL)
    {
        if (strcmp(current->key->doi, key->doi) == 0)
        {
            prev->next = current->next;
            List_free(current->value->authors);
            destroyNode(current);
            dic->size--;
            return 1;
        }
        prev = current;
        current = current->next;
    }

    return 0;
}

HashtableValueTP find(HashtableTP dic, HashtableKeyTP key)
{
    int hash = getHash(key);
    NodeTP entry = dic->table[hash];
    while (entry != NULL)
    {
        if (strcmp(entry->key->doi, key->doi) == 0)
        {
            return entry->value;
        }
        entry = entry->next;
    }
    return NULL;
}

int contains(HashtableTP dic, HashtableKeyTP key)
{
    HashtableValueTP m = find(dic, key);
    if (m == NULL)
        return 0;
    return 1;
}

void printTable(HashtableTP dic, List *authors)
{
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        NodeTP entry = dic->table[i];
        while (entry != NULL)
        {
            printf("%s, %s, %i, %i\n", entry->value->title, entry->value->journalName, entry->value->yearOfPublishing, entry->value->citingIndex);
            printf("Autors: %i\n", (int)List_size(entry->value->authors));
            for (int i = 0; i < (int)List_size(entry->value->authors); i++)
            {
                int index = atoi((char *)List_get(entry->value->authors, i));

                char *name = (char *)List_get(authors, index);
                printf("[%i] %s\n", i, name);
            }
            entry = entry->next;
        }
        puts("-----");
    }
}

void printInstance(HashtableValueTP m, List *authors)
{
    printf("%s, %s, %i, %i\n", m->title, m->journalName, m->yearOfPublishing, m->citingIndex);
    printf("Autors: %i\n", (int)List_size(m->authors));
    for (int i = 0; i < (int)List_size(m->authors); i++)
    {
        int index = atoi((char *)List_get(m->authors, i));

        char *name = (char *)List_get(authors, index);
        printf("[%i] %s\n", i, name);
    }
}

void deinitHashTable(HashtableTP dic)
{
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        NodeTP entry = dic->table[i];
        while (entry != NULL)
        {
            NodeTP tmpnode = entry;
            HashtableKeyTP tmpkey = entry->key;
            HashtableValueTP tmpvalue = entry->value;
            entry = entry->next;
            free(tmpkey);
            free(tmpvalue);
            free(tmpnode);
        }
    }
    free((HashtableTP)dic);
}

void destroyNode(NodeTP node)
{
    free(node->key);
    free(node->value);
    free(node);
}

List *CreateAuthorsList(PubTP key)
{
    List *list = List_alloc();

    return list;
}

void Addauthor(List *list1, NodeTP entry, char *authorinput)
{
    int ind = -1;
    for (int i = 0; i < List_size(list1); i++)
    {
        if (strcmp(List_get(list1, i), authorinput) == 0)
        {
            ind = i;
            break;
        }
    }
    if (ind >= 0)
    {
        char str[12];
        sprintf(str, "%d", ind);
        List_add(entry->value->authors, strOnHeap(str));
    }
    else
    {
        List_add(list1, strOnHeap(authorinput));
        char str[12];
        sprintf(str, "%d", (int)List_size(list1) - 1);
        List_add(entry->value->authors, strOnHeap(str));
    }
}
void FreeLists(HashtableTP dic)
{
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        NodeTP entry = dic->table[i];
        while (entry != NULL)
        {
            List_clear(entry->value->authors);
            List_free(entry->value->authors);
            entry = entry->next;
        }
    }
}

void authorsHIndex(HashtableTP dic, HashtableKeyTP key, List *authors)
{
    HashtableValueTP m = find(dic, key);
    if (m == NULL)
    {
        puts("No such publication!");
        return;
    }
    int authorsq = List_size(m->authors);
    for (int i = 0; i < authorsq; i++)
    {
        int size = 0;
        int indexes[100];
        char *authorind = List_get(m->authors, i);
        for (int j = 0; j < HASHTABLE_CAPACITY; j++)
        {
            NodeTP entry = dic->table[j];
            while (entry != NULL)
            {
                for (int t = 0; t < (int)List_size(entry->value->authors); t++)
                {
                    if (strcmp(authorind, List_get(entry->value->authors, t)) == 0)
                    {
                        indexes[size] = entry->value->citingIndex;
                        size++;
                    }
                }
                entry = entry->next;
            }
        }

        int newindexes[size];
        for (int j = 0; j < size; j++)
        {
            newindexes[j] = indexes[j];
        }
        bubbleSort(newindexes, size);

        int counter = size;
        int flag = 0;
        for (int j = size; j > 0; j--)
        {
            if (j <= newindexes[j - 1])
            {
                counter = j;
                flag = 1;
                break;
            }
        }
        counter = (flag == 0) ? 0 : counter;
        int index = atoi(authorind);
        char *name = (char *)List_get(authors, index);
        printf("%s: %i\n", name, counter);
    }
}

void reverse_array(int *pointer, int n)
{
    int *s, c, d;

    s = (int *)malloc(sizeof(int) * n);

    if (s == NULL)
        exit(EXIT_FAILURE);

    for (c = n - 1, d = 0; c >= 0; c--, d++)
        *(s + d) = *(pointer + c);

    for (c = 0; c < n; c++)
        *(pointer + c) = *(s + c);

    free(s);
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(int arr[], int n)
{
    int i, j;
    for (i = 0; i < n - 1; i++)
        for (j = 0; j < n - i - 1; j++)
            if (arr[j] < arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
}
