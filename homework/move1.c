#include <stdio.h>
#include <stdlib.h>
#define SIZE 5

int main()
{
    int arr[SIZE] = {1, 2, 3, 4, 5};
    int mat[SIZE];
    int k = 1;

    for(int i = 0; i < SIZE - k; i++)
    {
        mat[i + k] = arr[i];
    }

    for(int i = 0; i < k; i++)
    {
        mat[i] = arr[i + SIZE - k];
    }

    for(int i = 0; i < SIZE; i++)
    {
        printf("%i  ", mat[i]);
    }
    
    return 0;
}