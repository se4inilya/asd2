#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <assert.h>
#include <time.h>
#include <stdbool.h>

struct SLNode
{
    struct SLNode *next;
    int data;
};

struct DLNode
{
    struct DLNode *next;
    struct DLNode *prev;
    int data;
};

struct SLNode *createSLNode(int data);                                              //
struct DLNode *createDLNode(int data);                                              //
struct SLNode *addBackSLNode(struct SLNode *head, struct SLNode *node);             //
struct DLNode *addBackDLNode(struct DLNode *head, struct DLNode *node);             //
struct SLNode *addBegSLNode(struct SLNode *head, struct SLNode *node);              //
struct DLNode *addBegDLNode(struct DLNode *head, struct DLNode *node);              //
void printSLNode(struct SLNode *head);                                              //
void printDLNode(struct DLNode *head);                                              //
void SLDel(struct SLNode *head);                                                    //
void DLDel(struct DLNode *head);                                                    //
void delSLNode(struct SLNode *head, struct SLNode *node);                           //
struct DLNode *delDLNode(struct DLNode *head, struct DLNode *node);                 //
int slSize(struct SLNode *head);                                                    //
int dlSize(struct DLNode *head);                                                    //
int minElDLNode(struct DLNode *head);                                               //
struct DLNode *deleteElem(struct DLNode *list, int index);                          //
struct DLNode *connectLists(struct DLNode *list1, struct DLNode *lst2);             //
struct DLNode *swapMaxMinDLNode(struct DLNode *head);                               //
struct DLNode *swap(struct DLNode *lst1, struct DLNode *lst2, struct DLNode *head); //
struct DLNode *addIndDLNode(struct DLNode *head, struct DLNode *node, int ind);     //
void list_bubble_sort(struct DLNode *head);
void list_sample_sort(struct DLNode *head);
void list_insert_sort(struct DLNode *head);
static void sortedInsert(struct DLNode **head_ref, struct DLNode *newNode);
void sort_insertion(struct DLNode **head_ref);

int main()
{
    srand(time(0));

    struct DLNode *head2 = NULL;
    struct DLNode *head3 = NULL;

    for (int i = 0; i < 5; i++)
    {
        head2 = addBegDLNode(head2, createDLNode(i));
    }
    printf("Size: %i|  ", dlSize(head2));
    printDLNode(head2);
    for (int i = 0; i < 5; i++)
    {
        head3 = addBackDLNode(head3, createDLNode(i));
    }
    printf("Size: %i|  ", dlSize(head3));
    printDLNode(head3);

    printf("Connect two lists: ");
    head2 = connectLists(head2, head3);
    head2 = addIndDLNode(head2, createDLNode(10), 0);
    printDLNode(head2);

    printf("Swap min and max elements: ");
    head2 = swapMaxMinDLNode(head2);
    printDLNode(head2);

    printf("Sorted list: ");
    sort_insertion(&head2);
    printDLNode(head2);

    DLDel(head2);
    //DLDel(head3);

    return 0;
}

struct SLNode *createSLNode(int data)
{
    struct SLNode *head = malloc(sizeof(struct SLNode));
    head->next = NULL;
    head->data = data;
    return head;
}

struct DLNode *createDLNode(int data)
{
    struct DLNode *head = malloc(sizeof(struct DLNode));
    head->next = NULL;
    head->prev = NULL;
    head->data = data;
    return head;
}

struct SLNode *addBackSLNode(struct SLNode *head, struct SLNode *node)
{
    struct SLNode *iter = head;
    if (head == NULL)
        return node;

    while (iter->next)
    {
        iter = iter->next;
    }

    iter->next = node;
    node = iter;
    return head;
}

struct DLNode *addBackDLNode(struct DLNode *head, struct DLNode *node)
{
    if (head == NULL)
        return node;

    struct DLNode *iter = head;

    while (iter->next)
    {
        iter = iter->next;
    }

    iter->next = node;
    node->prev = iter;
    return head;
}

struct SLNode *addBegSLNode(struct SLNode *head, struct SLNode *node)
{
    if (head == NULL)
        return node;

    node->next = head;
    head = node;
    return head;
}

struct DLNode *addBegDLNode(struct DLNode *head, struct DLNode *node)
{
    if (head == NULL)
        return node;

    node->next = head;
    head->prev = node;
    return node;
}

void printSLNode(struct SLNode *head)
{
    struct SLNode *node = head;

    while (node)
    {
        printf("%i->", node->data);
        node = node->next;
    }
    printf("NULL\n");
}

void printDLNode(struct DLNode *head)
{
    struct DLNode *node = head;

    printf("NULL");

    while (node)
    {
        printf("<-%i->", node->data);
        node = node->next;
    }
    printf("NULL\n");
}

void SLDel(struct SLNode *head)
{
    struct SLNode *node = head;

    while (head)
    {
        node = head->next;
        free(head);
        head = node;
    }

    head = NULL;
}

void DLDel(struct DLNode *head)
{
    struct DLNode *node = head;

    while (head)
    {
        node = head->next;
        free(head);
        head = node;
    }

    head = NULL;
}

void delSLNode(struct SLNode *head, struct SLNode *node)
{
    struct SLNode *temp = head;

    if (temp == node)
    {
        struct SLNode *temp1 = head;
        if (temp1->next == NULL)
        {
            printf("Can not delete elem of single list\n");
            return;
        }
        temp1 = temp1->next;
        head->data = temp1->data;
        node->next = temp1->next;
        free(temp1);
        return;
    }

    while (temp->next != node)
    {
        temp = temp->next;
    }

    temp->next = node->next;
    free(node);
    return;
}

struct DLNode *delDLNode(struct DLNode *head, struct DLNode *node)
{
    struct DLNode *temp = head;

    if (temp == node)
    {
        struct DLNode *temp1 = head;
        if (temp1->next == NULL)
        {
            printf("Can not delete elem of single list\n");
            return temp1;
        }
        temp1 = temp1->next;
        head->data = temp1->data;
        struct DLNode *prev, *next;
        prev = temp1->prev;
        next = temp1->next;
        if (prev != NULL)
            prev->next = temp1->next;
        if (next != NULL)
            next->prev = temp1->prev;
        free(temp1);
        return head;
    }

    struct DLNode *prev, *next;
    prev = node->prev;
    next = node->next;
    if (prev != NULL)
        prev->next = node->next;
    if (next != NULL)
        next->prev = node->prev;
    free(node);
    return head;
}

int slSize(struct SLNode *head)
{
    struct SLNode *list = head;
    int counter = 0;
    while (list)
    {
        list = list->next;
        counter++;
    }
    return counter;
}

int dlSize(struct DLNode *head)
{
    struct DLNode *list = head;
    int counter = 0;
    while (list)
    {
        list = list->next;
        counter++;
    }
    return counter;
}

int minElDLNode(struct DLNode *head)
{
    if (head == NULL)
        return 0;

    struct DLNode *list = head;
    int min = list->data;
    while (list)
    {
        if (list->data < min)
            min = list->data;

        list = list->next;
    }
    return min;
}

struct DLNode *deleteElem(struct DLNode *list, int index)
{
    int cnt = 0;
    struct DLNode *iterator = list;
    while (iterator != NULL)
    {
        if (cnt == index)
        {
            struct DLNode *prev = iterator->prev;
            struct DLNode *next = iterator->next;
            if (prev == NULL)
            {
                free(iterator);
                next->prev = NULL;
                return next;
            }
            else
            {
                prev->next = next;
                if (next == NULL)
                {
                    prev->next = NULL;
                    free(iterator);
                    return list;
                }
                prev->next = next;
                next->prev = prev;
                free(iterator);
                return list;
            }
        }
        else
        {
            cnt++;
            iterator = iterator->next;
        }
    }
    return list;
}

struct DLNode *connectLists(struct DLNode *list1, struct DLNode *list2)
{
    struct DLNode *node1 = list1;

    while (node1->next)
    {
        node1 = node1->next;
    }

    //node1->next = list2;
    //node1->next->prev = node1; //ты доступаешься к некст (лист2), а от него доступаешься к прев (неопределенно (NULL))

    //struct DLNode *p = node1->next;
    node1->next = list2;
    list2->prev = node1;
    //list2->next = p;

    return list1;
}

struct el
{
    int maxEl;
    int minEl;
    int maxInd;
    int minInd;
};

struct DLNode *swapMaxMinDLNode(struct DLNode *head)
{
    struct DLNode *node = head;
    head = addBegDLNode(head, createDLNode(node->data));
    struct DLNode *list = head;
    struct el elem;
    elem.maxEl = list->data;
    elem.minEl = list->data;
    elem.maxInd = 0;
    elem.minInd = 0;

    int counter = 0;

    while (list)
    {
        if (list->data >= elem.maxEl)
        {
            elem.maxEl = list->data;
            elem.maxInd = counter;
        }

        if (list->data <= elem.minEl)
        {
            elem.minEl = list->data;
            elem.minInd = counter;
        }
        counter++;
        list = list->next;
    }

    list = head;
    counter = 0;

    struct DLNode *list2 = head;

    while (list)
    {
        if (counter == elem.minInd)
        {
            int counter2 = 0;

            while (list2)
            {
                if (counter2 == elem.maxInd)
                {
                    swap(list, list2, head);
                    head = deleteElem(head, 0);
                    return head;
                }
                counter2++;
                list2 = list2->next;
            }
        }

        if (counter == elem.maxInd)
        {
            int counter2 = 0;

            while (list2)
            {

                if (counter2 == elem.minInd)
                {
                    swap(list, list2, head);
                    head = deleteElem(head, 0);
                    return head;
                }
                counter2++;
                list2 = list2->next;
            }
        }

        list = list->next;
        counter++;
    }
    return head;
}

struct DLNode *swap(struct DLNode *lst1, struct DLNode *lst2, struct DLNode *head)
{
    // Возвращает новый корень списка
    struct DLNode *prev1, *prev2, *next1, *next2;
    prev1 = lst1->prev; // узел предшествующий lst1
    prev2 = lst2->prev; // узел предшествующий lst2
    next1 = lst1->next; // узел следующий за lst1
    next2 = lst2->next; // узел следующий за lst2
    if (lst2 == next1)  // обмениваются соседние узлы
    {
        lst2->next = lst1;
        lst2->prev = prev1;
        lst1->next = next2;
        lst1->prev = lst2;
        if (next2 != NULL)
            next2->prev = lst1;
        if (lst1 != head)
            prev1->next = lst2;
    }
    else if (lst1 == next2) // обмениваются соседние узлы
    {
        lst1->next = lst2;
        lst1->prev = prev2;
        lst2->next = next1;
        lst2->prev = lst1;
        if (next1 != NULL)
            next1->prev = lst2;
        if (lst2 != head)
            prev2->next = lst1;
    }
    else // обмениваются отстоящие узлы
    {
        if (lst1 != head)       // указатель prev можно установить только для элемента,
            prev1->next = lst2; // не являющегося корневым
        lst2->next = next1;
        if (lst2 != head)
            prev2->next = lst1;
        lst1->next = next2;
        lst2->prev = prev1;
        if (next2 != NULL)      // указатель next можно установить только для элемента,
            next2->prev = lst1; // не являющегося последним
        lst1->prev = prev2;
        if (next1 != NULL)
            next1->prev = lst2;
    }
    if (lst1 == head)
        return (lst2);
    if (lst2 == head)
        return (lst1);
    return (head);
}

struct DLNode *addIndDLNode(struct DLNode *head, struct DLNode *node, int ind)
{
    struct DLNode *list = head;

    int counter = 1;

    if (ind == 0)
    {
        addBegDLNode(head, node);
        return node;
    }

    while (list)
    {
        if (ind == counter)
        {
            struct DLNode *p = list->next;
            list->next = node;
            node->prev = list;
            node->next = p;

            return head;
        }

        list = list->next;
        counter++;
    }

    return head;
}

void list_bubble_sort(struct DLNode *head)
{
    struct DLNode *a = NULL;
    struct DLNode *b = NULL;
    struct DLNode *c = NULL;
    struct DLNode *e = NULL;
    struct DLNode *tmp = NULL;

    while (e != head->next)
    {
        c = a = head;
        b = a->next;
        while (a != e)
        {
            if (a->data > b->data)
            {
                if (a == head)
                {
                    tmp = b->next;
                    b->next = a;
                    a->next = tmp;
                    head = b;
                    c = b;
                }
                else
                {
                    tmp = b->next;
                    b->next = a;
                    a->next = tmp;
                    c->next = b;
                    c = b;
                }
            }
            else
            {
                c = a;
                a = a->next;
            }
            b = a->next;
            if (b == e)
                e = a;
        }
    }
}

//вставка и выбирка

void list_sample_sort(struct DLNode *head)
{
    struct DLNode *a = head;
    struct DLNode *b = head;
    struct DLNode *c = head;

    int counter = 0;

    while (a)
    {
        for (int i = 0; i < counter; i++)
        {
            b = b->next;
        }
        int min = minElDLNode(b);
        c = b;
        while (c)
        {
            if (c->data == min)
            {
                head = swap(b, c, head);
            }
            c = c->next;
        }
        b = head;
        a = a->next;
        counter++;
    }
}

void list_insert_sort(struct DLNode *head)
{
    // for (int i = 0; i < SIZE; i++)
    // {
    //     int j = i;
    //     while (arr[j] < arr[j - 1])
    //     {
    //         swap(&arr[j], &arr[j - 1]);
    //         j--;
    //     }
    // }

    struct DLNode *a = head;
    struct DLNode *b = head;
    struct DLNode *c = head;

    int counter = 1;

    a = a->next;
    printf("\n");

    while (a)
    {
        int count = counter;

        for (int i = 0; i < count; i++)
        {
            b = b->next;
            c = c->next;
        }
        //printf("%i\n", c->data);
        struct DLNode *p = c->prev;
        c = c->prev;
        printf("%i\n", p->data);

        while (b->data < c->data && c && b)
        {
            head = swap(b, c, head);

            if (c->prev->prev)
                c = c->prev->prev;
            else
                break;

            //b = b->prev;
        }

        a = a->next;
        b = head;
        c = head;
        counter++;
    }
}

static void sortedInsert(struct DLNode **head_ref, struct DLNode *newNode)
{
    struct DLNode *current;

    if (*head_ref == NULL)
    {
        *head_ref = newNode;
    }
    else if ((*head_ref)->data >= newNode->data)
    {
        newNode->next = *head_ref;
        newNode->next->prev = newNode;
        *head_ref = newNode;
    }
    else
    {
        current = *head_ref;
        while (current->next != NULL &&
               current->next->data < newNode->data)
            current = current->next;
        newNode->next = current->next;
        if (current->next != NULL)
            newNode->next->prev = newNode;
        current->next = newNode;
        newNode->prev = current;
    }
}

void sort_insertion(struct DLNode **head_ref)
{
    struct DLNode *sorted = NULL;
    struct DLNode *current = *head_ref;
    while (current != NULL)
    {
        struct DLNode *next = current->next;
        current->prev = current->next = NULL;
        sortedInsert(&sorted, current);
        current = next;
    }
    *head_ref = sorted;
}