#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define SIZE 10

int main()
{
    int arr[SIZE];
    int neg = 0;
    int pos = 0;
    int zero = 0;
    int max = 5;
    int min = -5;
    srand(time(0));

    for(int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for(int i = 0; i < SIZE; i++)
    {
        if(arr[i] > 0)
        {
            pos++;
        }

        if(arr[i] < 0)
        {
            neg++;
        }

        if(arr[i] == 0)
        {
            zero++;
        }
    }

    printf("\nPos: %i\n", pos);
    printf("\nNeg: %i\n", neg);
    printf("\nZERO: %i\n", zero);

    return 0;
}