#include <stdio.h>
#include <stdlib.h>
#define SIZE 6

int main()
{
    int arr[SIZE] = {1, 2, 3, 4, 5, 6};
    int k = 2;
    int arr1[k];
    int j = 0;

    for(int i = SIZE - k; i < SIZE; i++ ){
        arr1[j] = arr[i];
        j++;
    }
    for(int i = SIZE - k - 1; i >= 0; i--){
        arr[i + k] = arr[i];
    }
    for(int i = 0; i < k; i++){
        arr[i] = arr1[i];
    }

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }

    return 0;
}