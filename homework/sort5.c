// Shell's sort
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 10

int main()
{
    int arr[SIZE];
    srand(time(0));
    int max = 10;
    int min = -10;

    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n--------------------------------------\n");

    for (int gap = SIZE / 2; gap > 0; gap /= 2)
    {
        for (int i = gap; i < SIZE; i++)
        {
            int tmp = arr[i];
            int j = i;
            for (; j >= gap && tmp < arr[j - gap]; j -= gap)
            {
                arr[j] = arr[j - gap];
            }
            arr[j] = tmp;
        }
    }

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }
    printf("\n\n");

    return 0;
}