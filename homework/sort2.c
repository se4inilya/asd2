//сoртування вибіркою
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#define SIZE 10

void swap(int *arr1, int *arr2)
{
    int buf;
    buf = *arr1;
    *arr1 = *arr2;
    *arr2 = buf;
}

int main()
{
    int arr[SIZE];
    int leftB = 0;
    int *minEl;
    int min = -10;
    int max = 10;

    for (int i = 0; i < SIZE; i++)
    {
        arr[i] = rand() % (max - min + 1) + min;
        printf("%i  ", arr[i]);
    }
    printf("\n");

    for (int i = 0; i < SIZE; i++)
    {
        minEl = &arr[leftB];
        for (int j = leftB; j < SIZE; j++)
        {
            if (arr[j] < *minEl)
            {
                minEl = &arr[j];
            }
        }
        swap(&arr[leftB], minEl);
        leftB++;
    }

    for (int i = 0; i < SIZE; i++)
    {
        printf("%i  ", arr[i]);
    }
    printf("\n\n");

    return 0;
}